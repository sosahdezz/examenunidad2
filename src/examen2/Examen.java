/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen2;

/**
 *
 * @author REDES
 */
public class Examen {
    
            private int Codigo;
            private int Cantidad;
            private int Tipo;
            private float PrecioLitro;
            private float TotalPagar;
            // Esto es de cantidad de litros  NUMERO " 5 "
            private int CantidadL;
            
            public Examen (){
            
        this.Codigo = 0;
        this.Cantidad = 0;
        this.Tipo = 0;
        this.PrecioLitro = 0.0f;
        this.TotalPagar = 0.0f;
        this.CantidadL = 0;
        
            }
            
            
        public Examen (int Codigo,int Cantidad, int Tipo, float PrecioLitro, float TotalPagar, int CantidadL){
            this.Codigo = Codigo;
            this.Cantidad = Cantidad;
            this.Tipo = Tipo;
            this.PrecioLitro = PrecioLitro;
            this.TotalPagar = TotalPagar;
            this.CantidadL = CantidadL;
            
}

        public Examen (Examen otro){
            
            this.Codigo = otro.Codigo;
            this.Cantidad = otro.Cantidad;
            this.Tipo = otro.Tipo;
            this.PrecioLitro = otro.PrecioLitro;
            this.TotalPagar = otro.TotalPagar;
            this.CantidadL = otro.CantidadL;
           
        }
        
        //set y get
        

    public int getCodigo() {
        return Codigo;
    }

    public void setCodigo(int Codigo) {
        this.Codigo = Codigo;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    public int getTipo() {
        return Tipo;
    }

    public void setTipo(int Tipo) {
        this.Tipo = Tipo;
    }

    public float getPrecioLitro() {
        return PrecioLitro;
    }

    public void setPrecioLitro(float PrecioLitro) {
        this.PrecioLitro = PrecioLitro;
    }

    public float getTotalPagar() {
        return TotalPagar;
    }

    public void setTotalPagar(float TotalPagar) {
        this.TotalPagar = TotalPagar;
    }

    public int getCantidadL() {
        return CantidadL;
    }

    public void setCantidadL(int CantidadL) {
        this.CantidadL = CantidadL;
    }
        
         //METODOS DE COMPORTAMIENTO
    public float calcularPrecioxLitro(){
        float calTipo = 0.0f;
        
        if(Tipo == 1 ){
        calTipo = this.Tipo + 24.50f; 
     }
        if(Tipo == 2 ){
        calTipo = this.Tipo + 20.50f; 
     }
        return calTipo;
    }
    
    
    /*switch(Tipo){
              
              case 1: Tipo = 24.5; break;
              case 2: Tipo = 20.50f;
              
                  
          }*/
    
    public float calcularTotal(){
      float calTotal = 0.0f;
     // float cantidad =0.0f;
      
        calTotal = (this.CantidadL * this.PrecioLitro);
        return calTotal;
        
    }
        
}
